#!/usr/bin/python

"""
This is a simple example that demonstrates multiple links
between nodes.
"""

from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.node import OVSSwitch, Controller, RemoteController
from mininet.net import Mininet
from mininet.topo import Topo
from functools import partial

def runMultiLink():
    "Create and run multiple link network"
    topo = simpleMultiLinkTopo( n=1 )
    net = Mininet( topo=topo, controller=partial( RemoteController, ip='202.117.43.192', port=6633 ) )

    net.start()
    CLI( net )
    net.stop()


class simpleMultiLinkTopo( Topo ):
    "Simple topology with multiple links"

    def build( self, n, **_kwargs ):
        h1, h2, h3 = self.addHost( 'h1' ), self.addHost( 'h2' ), self.addHost( 'h3' )
        s1, s2 = self.addSwitch( 's1' ), self.addSwitch( 's2' )

        for _ in range( n ):
            self.addLink( s1, s2 )
            self.addLink( s1, h1 )
            self.addLink( s2, h2 )
            self.addLink( s2, h3 )

if __name__ == '__main__':
    setLogLevel( 'info' )
    runMultiLink()
