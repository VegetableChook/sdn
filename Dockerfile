FROM ubuntu:xenial

# install required packages
RUN apt-get clean
RUN apt-get update &&\
    apt-get install unzip -y &&\
    apt-get install net-tools -y &&\ 
    apt-get install iputils-ping -y &&\ 
    apt-get install vim -y &&\
    apt-get autoclean &&\
    apt-get autoremove

# install java
COPY jdk-8u241-linux-x64.tar.gz /java/
WORKDIR /java
RUN tar -zxvf jdk-8u241-linux-x64.tar.gz &&\
    rm jdk-8u241-linux-x64.tar.gz 

ENV JAVA_HOME=/java/jdk1.8.0_241
ENV JRE_HOME=$JAVA_HOME/jre 
ENV CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH
ENV PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
 
# install opendaylight
COPY opendaylight-0.12.0.zip /odl/
WORKDIR /odl
RUN unzip opendaylight-0.12.0.zip &&\
    rm opendaylight-0.12.0.zip

EXPOSE 8181 6653 6633 6640
